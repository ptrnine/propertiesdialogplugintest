#include "testplugin.h"
#include <KPluginMetaData>
#include <KPluginFactory>
#include <QDebug>

K_PLUGIN_CLASS_WITH_JSON(TestPlugin, "testplugin.json")

TestPlugin::TestPlugin(QObject *parent, const KPluginMetaData &metaData, const QList<QVariant> &)
    : KPropertiesDialogPlugin(qobject_cast<KPropertiesDialog *>(parent)) {
    qWarning() << "Hello from the test plugin!";
    qWarning() << "Plugin serviceTypes: " << metaData.serviceTypes();
    qWarning() << "Plugin mimeTypes: " << metaData.mimeTypes();
}

#include "testplugin.moc"


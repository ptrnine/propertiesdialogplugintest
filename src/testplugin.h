#ifndef TESTPLUGIN_H
#define TESTPLUGIN_H

#include <kpropertiesdialog.h>

class KPluginMetaData;

class TestPlugin : public KPropertiesDialogPlugin
{
    Q_OBJECT
public:
    TestPlugin(QObject *parent, const KPluginMetaData &metaData, const QList<QVariant> &args);
    ~TestPlugin() override = default;
    void applyChanges() override {}
};

#endif // TESTPLUGIN_H
